# Projet RWBYClicker

Ce projet est réalisé par Alexandre Mazeau et seulement lui.

RWBYClicker est un jeu en ligne style cookie clicker mais basé sur le thème de RWBY (web-série d'animation 3D).

## Installation

1. Clonez ce projet dans un dossier
2. Installez Docker si ce n'est pas déjà fait
3. À l'intérieur de ce dossier, tapez ces commandes :

```shell
docker-compose build
docker-compose up -d
docker-compose exec php bash
```

4. Une fois à l'intérieur du conteneur php grâce à la commande précédente, tapez les commandes suivantes :
```shell
composer install
composer require symfony/apache-pack
php bin/console doctrine:database:create --if-not-exists
php bin/console doctrine:schema:update --force
php bin/console doctrine:fixtures:load -n  
```

5. À partir d'ici vous pouvez aller sur votre navigateur préféré et taper l'url suivant afin de jouer au jeu :
```
localhost:8080
```

## Conteneur Mysql

1. Pour intéragir avec le conteneur, lancez la commande : 
```shell
docker-compose exec mysql bash
```
2. Pour accéder à la base de données, tapez les commandes suivantes en étant sur le conteneur:
```shell
mysql -u root -p
```
Le mot de passe sera celui indiqué dans le docker-compose.yml
```shell
use {nom de la base};
show tables;
```

C'est bon vous y êtes.
