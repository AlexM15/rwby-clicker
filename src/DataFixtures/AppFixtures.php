<?php

namespace App\DataFixtures;

use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use App\Entity\User;
use App\Entity\Monstre;

class AppFixtures extends Fixture
{
    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager)
    {
        $user1 = new User();
        $user1->setEmail("ruby.rose@rwby.com");
        $user1->setNickname("Ruby Rose");
        $user1->setRoles(["ROLE_ADMIN"]);
        $user1->setPassword(
            $this->encoder->encodePassword($user1,"RWBYCBIEN")
        );
        $user1->setDeadMonster(10000);
        $user1->setMoney(99999999);
        $manager->persist($user1);

        $user2 = new User();
        $user2->setEmail("mazeaualexandre1511@gmail.com");
        $user2->setNickname("AlexM");
        $user2->setPassword(
            $this->encoder->encodePassword($user2,"test")
        );
        $manager->persist($user2);

        $monstre1 = new Monstre();
        $monstre1->setName("Beowolf");
        $monstre1->setImg("Beowolf.png");
        $monstre1->setLP(10);
        $monstre1->setMoneyValue(10);
        $monstre1->setDescription("Beowolves have gone through several redesigns over the course of the series, but all incarnations share certain common traits. Consistently, Beowolves stand on their hind legs, albeit with a slouch, and are extremely muscular.
        <br><br>
        In the 'Red' Trailer, Beowolves appeared like pitch-black, featureless shadows in the shape of wolves, with red eyes and jaws.
        <br><br>
        The Volume 1 redesign of the creatures added the bone-like mask with red markings characteristic of most Grimm, as well as the bone-like spikes protruding from their arms, back, and knees. Their hands and feet are also tipped with long, sharp, white claws. They can vary greatly in size, with the average specimen being about the size of a human, but with larger Beowolves appearing that can be more than twice as large.
        <br><br>
        Older Beowolves are known as Alpha Beowolves, first appearing in 'Battle of Beacon', are covered in more bone-like armor and spines than the typical specimen. It has a more angular skull, larger teeth and sharp triangular ears.
        <br><br>
        In the 'Volume 4 Character Short', the Beowolves underwent another visual change, with a more angular head and more prominent ears, somewhat resembling the Alpha. Their glowing eyes also now leave a visible trail of red light as they move and are mostly yellow in color. Black smoke also seems to emanate from their bodies.
        <br><br>
        Merlot's Mutant Beowolves were similar to the Alpha, standing on their hind legs with massive green spikes on their back. They had white bone spots all over their body with a green tinge to them.
        <br><br>
        Most likely due to the fact that they inhabit the cold tundras of Solitas, the black skin of the northern variant of the Beowolf appears to be somewhat frozen and has white patches. Additionally, they have more spikes along their backs and arms. This variant is first seen in 'Ace Operatives' and is referred to as an Ice Beowolf.
        <br><br>
        They are first shown in the 'Red' Trailer as Ruby's opponents. They attack like a regular wolf would, with clawing and lunges, and no special skills or abilities demonstrated as of yet.
        <br><br>
        Their behavior varies depending on age. Younger Beowolves do not act like normal wolves, lacking any sense of self-preservation. They relentlessly attack no matter how injured they are or how many of their pack members are slain. Older Beowolves gain a sense of self-preservation like normal wolves and eventually become more intelligent than them, observing their allies, retreating when needed and developing strategies for future encounters against Huntsmen and other threats.
        <br><br>
        They are extremely agile, as seen when they stalked Ruby through the Cliffside Forest, as well as in their subsequent fight with her.
        <br><br>
        Alpha Beowolves are a much more dangerous, armored version of the Beowolf. Alphas are easily capable of destroying a pair of Atlesian Knight-200 androids, literally ripping them apart. Unlike the standard Beowolves which are easily dispatched in droves by trainee Huntsmen and Huntresses, this one was able to put up an intense, albeit brief, fight against Ironwood.");
        $manager->persist($monstre1);

        $monstre2 = new Monstre();
        $monstre2->setName("QueenLancer");
        $monstre2->setImg("QueenLancer.png");
        $monstre2->setLP(20);
        $monstre2->setMoneyValue(20);
        $monstre2->setDescription("Lancers have a three-segmented black body with three sets of legs. They have a white armored mask and armor along their back. On the back of their abdomen, the armor is in four overlapping plates. They have jagged pincers, antenna, four red transparent wings and a stinger.
        <br><br>
        The Lancer is capable of firing its stinger while keeping it attached to its body via an organic tether, functioning like a harpoon. The stinger is strong enough to pull apart the wooden frame of a Mistral airship as well as breaching a metal hull. A Queen Lancer has the added ability to fire out the spikes on its face. They also showed more intelligence compared to other Grimm, as they engaged in a coordinated attack. Some of them held down a ship, while one of them sacrificed itself to take it down.");
        $manager->persist($monstre2);

        $monstre3 = new Monstre();
        $monstre3->setName("Apathy");
        $monstre3->setImg("Apathy.png");
        $monstre3->setLP(40);
        $monstre3->setMoneyValue(50);
        $monstre3->setDescription("The Apathy is humanoid in shape, somewhat similar to the Imp, but are very tall, with long arms that reach down as far as the ground and extremely long fingers. They have hideous faces, resembling skulls, some even lacking nostrils, and low-hanging jaws.
        <br><br>
        There are at least four variants to the Apathy's appearance, two being extremely hunched back, whereas the others stand taller.
        <br><br>
        The Apathy has the ability to use a scream that weakens their prey by draining their will. This works through remote proximity, but if the Apathy can see their target, the effects become much stronger. If the prey is startled, they are jolted out of these remote effects, albeit temporarily.
        <br><br>
        Repeated attacks will weaken a target to the point of falling asleep while in combat, or even cause their death while unconscious. They also seem to have the ability to drain willpower from remote proximity, as shown when Team RWBY, Oscar Pine, Qrow Branwen and the former residents of Brunswick Farms were affected, in the latter case killing them in their sleep.
        <br><br>
        They tend to attack in packs, using their superior numbers and will-draining powers to bring down their prey and compensate for their lack of inherent physical strength. A single Apathy is less powerful and will take longer to drain willpower than it does for an entire pack of them.");
        $manager->persist($monstre3);

        $monstre4 = new Monstre();
        $monstre4->setName("Nevermore");
        $monstre4->setImg("Nevermore.png");
        $monstre4->setLP(60);
        $monstre4->setMoneyValue(70);
        $monstre4->setDescription("The Nevermore is an avian that has features resembling those of several other bird species. Two notable examples of this are its resemblances to condors and ravens. Like other Grimm, it has a mask-like, white bony structure overlaying the upper front of its head. It also has four glowing red eyes, two on each side of its head. The headpiece also includes a dorsal spine or fin, reminiscent of that on the heads of condors and some prehistoric pterosaurs.
        <br><br>
        Interestingly, the Nevermore is seen to have two clawed 'fingers' extending forwards from the outermost wing joint. This is a feature present in only a handful of birds on Earth, of which none are corvids. It is most closely matched by pseudo-fingers seen on the outer wing joints of ancient proto-birds like Archaeopteryx. The Nevermore is generally reminiscent of the Archaeopteryx in many ways, although it is many times larger, even in its smallest seen form.
        <br><br>
        In the 'Volume 4 Character Short', the Nevermore underwent a visual change, now with their red eyes changed into yellow, possess slightly longer wing claws, their beak is now pointed forward, their crest becomes shorter and pointed upward and their bone-like mask now has feathers sticking out. Like the designs of the Volume 4 Beowolf, black smoke also seems to emanate from their bodies.
        <br><br>
        Most likely due to the fact that they inhabit the cold tundras of Solitas, the black skin and feathers of the northern variant of the Nevermore appears to be somewhat frozen and has white patches. This variant is called the Ice Nevermore.
        <br><br>
        Whilst fully grown Nevermores are referred to as Giant Nevermores, their younger counterparts are called Nevermore Chicks. 
        <br><br>
        Nevermore appear to come in a large variety of sizes:
        <br><br>
        <ul> 
            <li>
                One variety close in size to small birds. Ruby crashes into one in 'The First Step, Pt.2', and Blake Belladonna later kills a number of them in 'Search and Destroy'.
            </li>
            <li>
                One slightly larger than an Ursa is shown in the Opening.
            </li>
            <li>
                The 'Giant Nevermore', the very large one which Team RWBY fights in Players and Pieces.
            </li>
            <li>
                Several Nevermore of an intermediate size appear in 'Breach'.
            </li>
        </ul>
        <br><br>
        The Nevermore seems to have the ability to easily swoop down for an attack, where it gains momentum in going straight back up. It's shown to have a tremendous amount of strength as seen when it was able to destroy a large stone structure with just one hit, as well as ramming itself into a stone bridge with little consequence. In 'Battle of Beacon' it has shown to be strong enough to break through the forcefield that protected Amity Colosseum.
        <br><br>
        Additionally, it is shown to be highly durable as it is hit several times by Team RWBY's attacks, yet appears to be unaffected and continues flying. Even when Yang shoots five blasts directly into its mouth with Ember Celica, it is only disoriented for a few seconds. Blake also hit it several times when on its back with no visible damage being dealt to the bird. This incredibly large Nevermore is only defeated after Ruby cuts through its neck with Crescent Rose whilst dragging it up a hundred-foot cliff-face with the help of her team.");
        $manager->persist($monstre4);

        $monstre5 = new Monstre();
        $monstre5->setName("Nukelavee");
        $monstre5->setImg("Nukelavee.png");
        $monstre5->setLP(80);
        $monstre5->setMoneyValue(100);
        $monstre5->setDescription("The Nuckelavee is a horse-like creature with an equine main body and a skinless humanoid creature that is fused with it. Its forelegs are shaped like claws and it has hooved hind legs that leave a characteristic hoofprint. The humanoid body has long curved horns on its head, a mouth that looks almost sewn shut, and two long elastic arms each with two clawed fingers. On the back of the human torso are boney spikes along its spine and a variety of weapons from previous battles stuck in it.
        <br><br>
        When walking on its equine legs, its attached humanoid portion hangs limply to one side, making its arms drag along the ground. Once the equine portion stops to stand still, only then does the humanoid portion of the creature become active, doing so with a spasmodic twitching movement. When enraged, the spikes on its spine lengthen, as do its horns, its pupils become vertical slits, and its mouth opens completely.
        <br><br>
        The Nuckelavee has been credited with the destruction of Oniyuri and played a role in the destruction of both Shion and Kuroyuri. Despite its jerky movements, it is quite fast and uses its unnaturally stretchy limbs to attack. Its screams are also very discomforting to the ear. It can also rotate its humanoid torso indefinitely without stretching, twisting, or breaking its attachment to the horse, as seen when it spins around while whipping its arms at RNJR.
        <br><br>
        The Nuckelavee is far more durable than its size and frail appearance would indicate, shrugging off dozens of rounds from Magnhild's grenades, Crescent Rose's rifle shots, and Stormflower's bullets in addition to slashes from the upgraded Crocea Mors. Its arms and heads were more vulnerable, but the creature's long-range and vast strength made attacking them difficult.
        <br><br>
        Possibly due to its longevity, this creature possesses a degree of intelligence not commonly found in other Grimm. It had a lair in which it discarded many of the weapons embedded in it over its life, as well as trophy-like collections such as a flag from Shion, a town many miles away whose destruction it participated in. It was able to think and strategize, countering Jaune's encircling tactics by spinning its arms in a wide circle to strike all its opponents at once.
        <br><br>
        However, in the Volume 4 Crew Commentary, it has been stated that only either the horse or the 'Imp' can move at a time.");
        $manager->persist($monstre5);

        $monstre6 = new Monstre();
        $monstre6->setName("Wyvern");
        $monstre6->setImg("Wyvern.png");
        $monstre6->setLP(100);
        $monstre6->setMoneyValue(150);
        $monstre6->setDescription("The Wyvern is an enormous Grimm, with large red bat-like wings, a pair of legs and a tail. The red membrane of the wing is translucent. It has rough black skin, with white bone-like spines along its body, including an external skull, rib cage and rows of dorsal spikes.
        <br><br>
        In it has large claws on both its legs and on the tip of its tail. The Wyvern's skull has a pair of large prominent horns and six yellow-red eyes. Its jaw has teeth extending down along the creature's neck, beyond its skull, such that its entire neck also opens when it roars.
        <br><br>
        Although it has yet to be seen in combat, the Wyvern is capable of flight despite its size. It has the ability to summon other Grimm: the creature exudes a black, incredibly viscous tar-like substance from the skin all over its body. When droplets of this substance impact the ground, lesser forms of Grimm rise from the residue. Types of Grimm which were observed to rise out of the liquid include Creeps, Beowolves and Ursai, and Sabyrs.
        <br><br>
        The Wyvern's behavior could be influenced by the powers of the Fall Maiden, as seen when Cinder controls the beast into demolishing Beacon Tower.");
        $manager->persist($monstre6);

        $manager->flush();
    }
}
