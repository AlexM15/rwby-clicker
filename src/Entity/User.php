<?php

namespace App\Entity;

use App\Repository\UserRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass=UserRepository::class)
 */
class User implements UserInterface
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     */
    private $email;

    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     */
    private $password;

    /**
     * @ORM\Column(type="integer")
     */
    private $atlesianKnight130 = 0;

    /**
     * @ORM\Column(type="integer")
     */
    private $atlesianKnight200 = 0;

    /**
     * @ORM\Column(type="integer")
     */
    private $atlesianPaladin290 = 0;

    /**
     * @ORM\Column(type="integer")
     */
    private $spiderDroid = 0;

    /**
     * @ORM\Column(type="integer")
     */
    private $aP06 = 0;

    /**
     * @ORM\Column(type="integer")
     */
    private $colossus = 0;

    /**
     * @ORM\Column(type="integer")
     */
    private $money = 0;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $deadMonster = 0;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nickname;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function getRolesString(): string
    {
        $roles = $this->roles;
        $string = "";

        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        foreach ($roles as $key => $role) {
            if($key == count($roles)-1){
                $string .= $role;
            }
            else{
                $string .= $role.";";
            }
        }

        return $string;
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string) $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getAtlesianKnight130(): ?int
    {
        return $this->atlesianKnight130;
    }

    public function setAtlesianKnight130(int $atlesianKnight130): self
    {
        $this->atlesianKnight130 = $atlesianKnight130;

        return $this;
    }

    public function getAtlesianKnight200(): ?int
    {
        return $this->atlesianKnight200;
    }

    public function setAtlesianKnight200(int $atlesianKnight200): self
    {
        $this->atlesianKnight200 = $atlesianKnight200;

        return $this;
    }

    public function getAtlesianPaladin290(): ?int
    {
        return $this->atlesianPaladin290;
    }

    public function setAtlesianPaladin290(int $atlesianPaladin290): self
    {
        $this->atlesianPaladin290 = $atlesianPaladin290;

        return $this;
    }

    public function getSpiderDroid(): ?int
    {
        return $this->spiderDroid;
    }

    public function setSpiderDroid(int $spiderDroid): self
    {
        $this->spiderDroid = $spiderDroid;

        return $this;
    }

    public function getAP06(): ?int
    {
        return $this->aP06;
    }

    public function setAP06(int $aP06): self
    {
        $this->aP06 = $aP06;

        return $this;
    }

    public function getColossus(): ?int
    {
        return $this->colossus;
    }

    public function setColossus(int $colossus): self
    {
        $this->colossus = $colossus;

        return $this;
    }

    public function getMoney(): ?int
    {
        return $this->money;
    }

    public function setMoney(int $money): self
    {
        $this->money = $money;

        return $this;
    }

    public function getDeadMonster(): ?int
    {
        return $this->deadMonster;
    }

    public function setDeadMonster(?int $deadMonster): self
    {
        $this->deadMonster = $deadMonster;

        return $this;
    }

    public function getNickname(): ?string
    {
        return $this->nickname;
    }

    public function setNickname(string $nickname): self
    {
        $this->nickname = $nickname;

        return $this;
    }   
}
