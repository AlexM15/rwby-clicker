<?php

namespace App\Entity;

use App\Repository\MonstreRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=MonstreRepository::class)
 */
class Monstre
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $img;

    /**
     * @ORM\Column(type="integer")
     */
    private $lP;

    /**
     * @ORM\Column(type="integer")
     */
    private $moneyValue;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=4500)
     */
    private $description;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getImg(): ?string
    {
        return $this->img;
    }

    public function setImg(string $img): self
    {
        $this->img = $img;

        return $this;
    }

    public function getLP(): ?int
    {
        return $this->lP;
    }

    public function setLP(int $lP): self
    {
        $this->lP = $lP;

        return $this;
    }

    public function getMoneyValue(): ?int
    {
        return $this->moneyValue;
    }

    public function setMoneyValue(int $moneyValue): self
    {
        $this->moneyValue = $moneyValue;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }
}
