<?php

namespace App\Controller;

use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\User;
use App\Entity\Monstre;

class ClickerController extends AbstractController
{
    /**
     * @Route("/", name="clicker")
     */
    public function index(): Response
    {
        return $this->render('clicker/index.html.twig', [
            'controller_name' => 'ClickerController',
        ]);
    }

    /**
     * @Route("/save", name="clicker_save")
     */
    public function save(Request $request): Response
    {
        try {
            $user = $this->getUser();

            $user->setAtlesianKnight130(intval($request->request->get('AtlesianKnight130')));
            $user->setAtlesianKnight200(intval($request->request->get('AtlesianKnight200')));
            $user->setAtlesianPaladin290(intval($request->request->get('AtlesianPaladin290')));
            $user->setSpiderDroid(intval($request->request->get('SpiderDroid')));
            $user->setAP06(intval($request->request->get('AP06')));
            $user->setColossus(intval($request->request->get('Colossus')));
            $user->setMoney(intval($request->request->get('Money')));
            $user->setDeadMonster(intval($request->request->get('DeadMonster')));

            $this->getDoctrine()->getManager()->persist($user);
            $this->getDoctrine()->getManager()->flush();

            return new Response("sauvegarde effectuée !");
        } catch (Exception $e) {
            return new Response($e, 404);
        }
    }

    /**
     * @Route("/getinfos", name="clicker_infos")
     */
    public function getInfos(Request $request, SerializerInterface $serializer): Response
    {
        try {
            $user = $this->getUser();

            $userJson = $serializer->serialize($user, 'json');

            return new Response($userJson);
        } catch (Exception $e) {
            return new Response($e, 404);
        }
    }

    /**
     * @Route("/getmonstres", name="clicker_monstres")
     */
    public function getMonstres(Request $request, SerializerInterface $serializer): Response
    {
        try {
            $monstres = $this->getDoctrine()->getRepository(Monstre::class)->findAll();

            $listMonstreJson = $serializer->serialize($monstres, 'json');

            return new Response($listMonstreJson);
        } catch (Exception $e) {
            return new Response($e, 404);
        }
    }

    /**
     * @Route("/profile", name="clicker_profile")
     */
    public function profileConnectedUser(Request $request): Response
    {
        $user = $this->getUser();

        return $this->render('clicker/profile.html.twig', [
            'controller_name' => 'ClickerController',
            'user' => $user,
            'same' => true
        ]);
    }

    /**
     * @Route("/profile/{id}", name="clicker_profile_other")
     */
    public function profileOtherUser(Request $request, int $id): Response
    {
        $same = false;
        $user = $this->getDoctrine()->getRepository(User::class)->find($id);

        if ($user == null) {
            return $this->render('bundles/TwigBundle/Exception/error404.html.twig', [
                'controller_name' => 'ClickerController'
            ]);
        }

        if ($user == $this->getUser()) {
            $same = true;
        }

        return $this->render('clicker/profile.html.twig', [
            'controller_name' => 'ClickerController',
            'user' => $user,
            'same' => $same
        ]);
    }

    /**
     * @Route("/bestiary", name="clicker_bestiary")
     */
    public function bestiary(Request $request): Response
    {
        $monstres = $this->getDoctrine()->getRepository(Monstre::class)->findAll();

        return $this->render('clicker/bestiary/list.html.twig', [
            'controller_name' => 'ClickerController',
            'monstres' => $monstres
        ]);
    }

    /**
     * @Route("/bestiary/{id}", name="clicker_bestiary_detail")
     */
    public function bestiaryDetail(Request $request, int $id): Response
    {
        $monstre = $this->getDoctrine()->getRepository(Monstre::class)->find($id);

        if ($monstre == null) {
            return $this->redirectToRoute('clicker_bestiary');
        }

        return $this->render('clicker/bestiary/detail.html.twig', [
            'controller_name' => 'ClickerController',
            'monstre' => $monstre
        ]);
    }

    /**
     * @Route("/leaderboard", name="clicker_leaderboard")
     */
    public function leaderboard(Request $request): Response
    {
        if ($request->isMethod('POST')) {
            if ($request->request->get('sortOption') == 'dead') {
                $sort = 'dead';
                $users = $this->getDoctrine()->getRepository(User::class)->findUserDeadOrderDesc();
            } else {
                $sort = 'money';
                $users = $this->getDoctrine()->getRepository(User::class)->findUserMoneyOrderDesc();
            }
        } else {
            $sort = 'dead';
            $users = $this->getDoctrine()->getRepository(User::class)->findUserDeadOrderDesc();
        }

        return $this->render('clicker/leaderboard.html.twig', [
            'controller_name' => 'ClickerController',
            'users' => $users,
            'sort' => $sort,
            'maxElement' => 10
        ]);
    }
}
