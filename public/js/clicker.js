
/////////////////////////////////////////////////////////////////
//                       VARIABLES                             //
/////////////////////////////////////////////////////////////////

//variables pour les clics
var ClickParSeconde = 0

//variables pour l'argent
var Money = 0

//variables pour les monstres
var Monstres = []
var LPMonstreFull = 0
var LPMonstre = 0
var MoneyValueMonstre = 0
var IsAlive = false

//variables pour les améliorations
var AtlesianKnight130Unlocked = false
var AtlesianKnight130Lvl = 0
var AtlesianKnight200Unlocked = false
var AtlesianKnight200Lvl = 0
var AtlesianPaladin290Unlocked = false
var AtlesianPaladin290Lvl = 0
var SpiderDroidUnlocked = false
var SpiderDroidLvl = 0
var AP06Unlocked = false
var AP06Lvl = 0
var ColossusUnlocked = false
var ColossusLvl = 0

//variables pour l'utilisateur
var InfosCookie = ""
var WantCookie = "no"
var Connected = false
var User = ""
var DeadMonster = 0
var Resolution = ""

//variables reset
var Reset = false

/////////////////////////////////////////////////////////////////
//                    INITIALISATION                           //
/////////////////////////////////////////////////////////////////

//initialisation des textes des boutons des améliorations
function initialiseTheGame() {

    WantCookie = getCookie('want_cookie')

    // Si l'utilisateur a accepté l'utilisation des cookies
    if (WantCookie == "yes") {
        document.getElementById("checkcookies").innerHTML = "cookies alive";
        document.getElementById("cookie-bar").style.display = 'none';
    }
    else if (WantCookie == "no") {
        document.getElementById("checkcookies").innerHTML = "cookies blocked";
        document.getElementById("cookie-bar").style.display = 'none';
    }
    else {
        document.getElementById("cookie-bar").style.display = "block";
    }

    document.getElementById("checkcookies").addEventListener("click", showCookiebar);
    document.getElementById("agree").addEventListener("click", hideCookiebarAgree);
    document.getElementById("stop-cookie").addEventListener("click", hideCookiebarStop);

    //Lorsque que le jeu est quitté, on sauvegarde les données de l'utilisateur
    window.onbeforeunload = function () {
        sauvegarde()
    }

    if (window.screen.width >= 992) {
        Resolution = "computer_"
    }
    else {
        Resolution = "mobile_"
    }

    window.addEventListener('resize', () => {
        if (window.screen.width >= 992) {
            Resolution = "computer_"
            rewriteUpgrades()
        }
        else {
            Resolution = "mobile_"
            rewriteUpgrades()
        }
    });

    getUserInfos()

    getMonstres()
}

/////////////////////////////////////////////////////////////////
//                       OUTILS                                //
/////////////////////////////////////////////////////////////////

function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function showCookiebar() {
    document.getElementById("cookie-bar").style.display = 'block';
}

function hideCookiebarAgree() {
    setCookie("want_cookie", "yes", 365)
    document.getElementById("checkcookies").innerHTML = "cookies alive";
    document.getElementById("cookie-bar").style.display = 'none';
}


function hideCookiebarStop() {
    setCookie("want_cookie", "no", 365)
    document.getElementById("checkcookies").innerHTML = "cookies blocked";
    document.getElementById("cookie-bar").style.display = 'none';
}

//calcul le nombre de click par seconde automatique (venant des améliorations)
function calculCPS() {
    ClickParSeconde = (5 * AtlesianKnight200Lvl) + (10 * AtlesianPaladin290Lvl) + (100 * SpiderDroidLvl) + (1000 * AP06Lvl) + (10000 * ColossusLvl)
    document.getElementById(Resolution.concat('', "clic")).innerHTML = "Clicks per second : " + nFormatter(ClickParSeconde, 3)
}

//inflige les dégats automatique au monstre     
function degatMonstre() {
    if (IsAlive) {
        calculCPS()
        if (ClickParSeconde <= LPMonstre) {
            LPMonstre -= ClickParSeconde
        }
        else {
            LPMonstre = 0
            mortMonstre()
        }
        document.getElementById("computer_LPMonstre").style.width = LPMonstre / LPMonstreFull * 100 + "%"
        document.getElementById("mobile_LPMonstre").style.width = LPMonstre / LPMonstreFull * 100 + "%"
    }
}

//ajout argent suite à la mort du monstre
function mortMonstre() {
    Money = Money + MoneyValueMonstre
    DeadMonster = DeadMonster + 1
    checkAccesButton()
    newMonstre()
    document.getElementById("computer_money").innerHTML = "Money : " + nFormatter(Money, 3)
    document.getElementById("mobile_money").innerHTML = "Money : " + nFormatter(Money, 3)
    document.getElementById("computer_monstre").innerHTML = "Killed Monsters : " + nFormatter(DeadMonster, 3)
    document.getElementById("mobile_monstre").innerHTML = "Killed Monsters : " + nFormatter(DeadMonster, 3)
}

//check toutes les secondes
var i = setInterval(function () { degatMonstre(); }, 1000);

//formate les chiffres
function nFormatter(num, digits) {
    var si = [
        { value: 1, symbol: "" },
        { value: 1E3, symbol: "k" },
        { value: 1E6, symbol: "M" },
        { value: 1E9, symbol: "G" },
        { value: 1E12, symbol: "T" },
        { value: 1E15, symbol: "P" },
        { value: 1E18, symbol: "E" }
    ];
    var rx = /\.0+$|(\.[0-9]*[1-9])0+$/;
    var i;
    for (i = si.length - 1; i > 0; i--) {
        if (num >= si[i].value) {
            break;
        }
    }
    return (num / si[i].value).toFixed(digits).replace(rx, "$1") + si[i].symbol;
}

//check si suffisamment d'argent pour débloquer/upgrade (active les boutons si oui)
function checkAccesButton() {
    var list = [
        [AtlesianKnight130Lvl, 'buttonAtlesianKnight130', 10],
        [AtlesianKnight200Lvl, 'buttonAtlesianKnight200', 1000],
        [AtlesianPaladin290Lvl, 'buttonAtlesianPaladin290', 100000],
        [SpiderDroidLvl, 'buttonSpiderDroid', 10000000],
        [AP06Lvl, 'buttonAP06', 1000000000],
        [ColossusLvl, 'buttonColossus', 100000000000]
    ]

    list.forEach(upgrade => {
        if (Money >= (upgrade[0] + 1) * upgrade[2]) {
            document.getElementById(Resolution.concat('', upgrade[1])).disabled = false;
        }
        else {
            document.getElementById(Resolution.concat('', upgrade[1])).disabled = true;
        }
    })
}

function rewriteUpgrades() {
    var ameliorations = [
        ["AtlesianKnight130", AtlesianKnight130Unlocked, "nombreAtlesianKnight130", 10, AtlesianKnight130Lvl, 1, "divAtlesianKnight130", "buttonAtlesianKnight130"],
        ["AtlesianKnight200", AtlesianKnight200Unlocked, "nombreAtlesianKnight200", 1000, AtlesianKnight200Lvl, 5, "divAtlesianKnight200", "buttonAtlesianKnight200"],
        ["AtlesianPaladin290", AtlesianPaladin290Unlocked, "nombreAtlesianPaladin290", 100000, AtlesianPaladin290Lvl, 10, "divAtlesianPaladin290", "buttonAtlesianPaladin290"],
        ["SpiderDroid", SpiderDroidUnlocked, "nombreSpiderDroid", 10000000, SpiderDroidLvl, 100, "divSpiderDroid", "buttonSpiderDroid"],
        ["AP06", AP06Unlocked, "nombreAP06", 1000000000, AP06Lvl, 1000, "divAP06", "buttonAP06"],
        ["Colossus", ColossusUnlocked, "nombreColossus", 100000000000, ColossusLvl, 10000, "divColossus", "buttonColossus"],
    ]

    ameliorations.forEach(amelioration => {
        if (amelioration[4] != 0) {
            amelioration[1] = true
            document.getElementById(Resolution.concat('', amelioration[6])).style.display = 'block'
            document.getElementById(Resolution.concat('', amelioration[7])).innerHTML = "Upgrade for " + nFormatter((amelioration[4] + 1) * amelioration[3], 3)
            if (amelioration[0] == "AtlesianKnight130") {
                document.getElementById(Resolution.concat('', amelioration[2])).innerHTML = "+" + nFormatter(amelioration[4] * amelioration[5], 3) + " clicks per click"
            }
            else {
                document.getElementById(Resolution.concat('', amelioration[2])).innerHTML = "+" + nFormatter(amelioration[4] * amelioration[5], 3) + " clicks per second"
            }
        }
        else {
            document.getElementById(Resolution.concat('', amelioration[6])).style.display = 'none'
            document.getElementById(Resolution.concat('', amelioration[7])).innerHTML = "Unlock pour " + nFormatter((amelioration[4] + 1) * amelioration[3], 3)
        }
    });

    AtlesianKnight130Unlocked = ameliorations[0][1]
    AtlesianKnight200Unlocked = ameliorations[1][1]
    AtlesianPaladin290Unlocked = ameliorations[2][1]
    SpiderDroidUnlocked = ameliorations[3][1]
    AP06Unlocked = ameliorations[4][1]
    ColossusUnlocked = ameliorations[5][1]
}

function getUserInfos() {
    document.getElementById("spinner").style.display = 'initial';
    //requete pour l'utilisateur connecté
    var http_request = false;
    //créer une instance (un objet) de la classe désirée fonctionnant sur plusieurs navigateurs
    if (window.XMLHttpRequest) { // Mozilla, Safari,...
        http_request = new XMLHttpRequest();
        if (http_request.overrideMimeType) {
            http_request.overrideMimeType('text/xml');//un appel de fonction supplémentaire pour écraser l'en-tête envoyé par le serveur, juste au cas où il ne s'agit pas de text/xml, pour certaines versions de navigateurs Mozilla
        }
    } else if (window.ActiveXObject) { // IE
        try {
            http_request = new ActiveXObject("Msxml2.XMLHTTP");
        } catch (e) {
            try {
                http_request = new ActiveXObject("Microsoft.XMLHTTP");
            } catch (e) { }
        }
    }

    if (!http_request) {
        document.getElementById("spinner").style.display = 'none';
        console.log('Abandon :( Impossible de créer une instance XMLHTTP');
        return false;
    }
    http_request.onreadystatechange = function () { traitementReponseGetUser(http_request); } //affectation fonction appelée qd on recevra la reponse
    // lancement de la requete
    http_request.open('POST', "http://localhost:8080/getinfos", true);
    //changer le type MIME de la requête pour envoyer des données avec la méthode POST ,  !!!! cette ligne doit etre absolument apres http_request.open('POST'....
    http_request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');

    http_request.send();
}

function getMonstres() {
    document.getElementById("spinner").style.display = 'initial';
    //requete pour les monstres
    var http_request = false;
    //créer une instance (un objet) de la classe désirée fonctionnant sur plusieurs navigateurs
    if (window.XMLHttpRequest) { // Mozilla, Safari,...
        http_request = new XMLHttpRequest();
        if (http_request.overrideMimeType) {
            http_request.overrideMimeType('text/xml');//un appel de fonction supplémentaire pour écraser l'en-tête envoyé par le serveur, juste au cas où il ne s'agit pas de text/xml, pour certaines versions de navigateurs Mozilla
        }
    } else if (window.ActiveXObject) { // IE
        try {
            http_request = new ActiveXObject("Msxml2.XMLHTTP");
        } catch (e) {
            try {
                http_request = new ActiveXObject("Microsoft.XMLHTTP");
            } catch (e) { }
        }
    }

    if (!http_request) {
        document.getElementById("spinner").style.display = 'none';
        console.log('Abandon :( Impossible de créer une instance XMLHTTP');
        return false;
    }
    http_request.onreadystatechange = function () { traitementReponseGetMonster(http_request); } //affectation fonction appelée qd on recevra la reponse
    // lancement de la requete
    http_request.open('POST', "http://localhost:8080/getmonstres", true);
    //changer le type MIME de la requête pour envoyer des données avec la méthode POST ,  !!!! cette ligne doit etre absolument apres http_request.open('POST'....
    http_request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');

    http_request.send();
}

function sauvegarde() {
    if (Connected) {
        var http_request = false;
        //créer une instance (un objet) de la classe désirée fonctionnant sur plusieurs navigateurs
        if (window.XMLHttpRequest) { // Mozilla, Safari,...
            http_request = new XMLHttpRequest();
            if (http_request.overrideMimeType) {
                http_request.overrideMimeType('text/xml');//un appel de fonction supplémentaire pour écraser l'en-tête envoyé par le serveur, juste au cas où il ne s'agit pas de text/xml, pour certaines versions de navigateurs Mozilla
            }
        } else if (window.ActiveXObject) { // IE
            try {
                http_request = new ActiveXObject("Msxml2.XMLHTTP");
            } catch (e) {
                try {
                    http_request = new ActiveXObject("Microsoft.XMLHTTP");
                } catch (e) { }
            }
        }

        if (!http_request) {
            console.log('Abandon :( Impossible de créer une instance XMLHTTP');
            return false;
        }
        http_request.onreadystatechange = function () { traitementReponseSauvegarde(http_request); } //affectation fonction appelée qd on recevra la reponse
        // lancement de la requete
        http_request.open('POST', "http://localhost:8080/save", true);
        //changer le type MIME de la requête pour envoyer des données avec la méthode POST ,  !!!! cette ligne doit etre absolument apres http_request.open('POST'....
        http_request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');

        var data = "AtlesianKnight130=" + AtlesianKnight130Lvl +
            "&AtlesianKnight200=" + AtlesianKnight200Lvl +
            "&AtlesianPaladin290=" + AtlesianPaladin290Lvl +
            "&SpiderDroid=" + SpiderDroidLvl +
            "&AP06=" + AP06Lvl +
            "&Colossus=" + ColossusLvl +
            "&Money=" + Money +
            "&DeadMonster=" + DeadMonster;

        http_request.send(data);
    }
    else {
        console.log("no user connected")
    }
    // Si l'utilisateur n'a pas refusé l'utilisation des cookies
    WantCookie = getCookie("want_cookie")
    if (WantCookie == "yes") {
        setCookie('save', '{"AtlesianKnight130" : ' + AtlesianKnight130Lvl + ', "AtlesianKnight200" : ' + AtlesianKnight200Lvl + ', "AtlesianPaladin290" : ' + AtlesianPaladin290Lvl + ', "SpiderDroid" : ' + SpiderDroidLvl + ', "AP06" : ' + AP06Lvl + ', "Colossus" : ' + ColossusLvl + ', "Money" : ' + Money + ', "DeadMonster" : ' + DeadMonster + '}', 365)
        // je sauvegarde les données de la partie de l'utilisateur toutes les 10min 
        console.log("save cookie")
    }
    else {
        console.log("no save cookie")
    }

    if (!Connected) {
        initialiseTheGame()
    }
}

//check toutes les secondes
var y = setInterval(function () { sauvegarde(); }, 300000);

function traitementReponseGetUser(http_request) {
    if (http_request.readyState == 4) {
        if (http_request.status == 200) {
            // cas avec reponse de PHP en mode texte:
            //chargement des elements reçus dans la liste
            var affich_rep = http_request.responseText

            InfosCookie = getCookie('save')
            WantCookie = getCookie('want_cookie')

            if (affich_rep != "null") {
                User = JSON.parse(affich_rep)
                Connected = true

                //savoir si cookie ou BDD priorité
                if (checkSave() == "cookie" || WantCookie == "yes") {

                    AtlesianKnight130Lvl = InfosCookie.AtlesianKnight130
                    AtlesianKnight200Lvl = InfosCookie.AtlesianKnight200
                    AtlesianPaladin290Lvl = InfosCookie.AtlesianPaladin290
                    SpiderDroidLvl = InfosCookie.SpiderDroid
                    AP06Lvl = InfosCookie.AP06
                    ColossusLvl = InfosCookie.Colossus
                    Money = InfosCookie.Money
                    DeadMonster = InfosCookie.DeadMonster
                    console.log('Data have been recovered from cookies !');
                }
                else {
                    AtlesianKnight130Lvl = User.atlesianKnight130
                    AtlesianKnight200Lvl = User.atlesianKnight200
                    AtlesianPaladin290Lvl = User.atlesianPaladin290
                    SpiderDroidLvl = User.spiderDroid
                    AP06Lvl = User.aP06
                    ColossusLvl = User.colossus
                    Money = User.money
                    DeadMonster = User.deadMonster
                    console.log('Data have been recovered from the Hunter account !');
                }

                document.getElementById("spinner").style.display = 'none';
            }
            else if (InfosCookie != "" && WantCookie == "yes") {
                InfosCookie = JSON.parse(InfosCookie)

                AtlesianKnight130Lvl = InfosCookie.AtlesianKnight130
                AtlesianKnight200Lvl = InfosCookie.AtlesianKnight200
                AtlesianPaladin290Lvl = InfosCookie.AtlesianPaladin290
                SpiderDroidLvl = InfosCookie.SpiderDroid
                AP06Lvl = InfosCookie.AP06
                ColossusLvl = InfosCookie.Colossus
                Money = InfosCookie.Money
                DeadMonster = InfosCookie.DeadMonster
                console.log('Data have been recovered from cookies !');
            }
            else {
                console.log("The user is not connected, do not have a save or do not authorize access for the cookies and must to begin a new game");
            }
            document.getElementById("spinner").style.display = 'none';

            rewriteUpgrades()

            isfinished = true

            checkAccesButton()
            calculCPS()
            document.getElementById(Resolution.concat('', "money")).innerHTML = "Money : " + nFormatter(Money, 3)
            document.getElementById(Resolution.concat('', "monstre")).innerHTML = "Killed Monsters : " + nFormatter(DeadMonster, 3)
        }
        else {
            document.getElementById("spinner").style.display = 'none';
            console.log('A problem has occured with getting the Hunter actually connected : ' + http_request.status);
        }
    }
}

function traitementReponseGetMonster(http_request) {
    if (http_request.readyState == 4) {
        if (http_request.status == 200) {
            // cas avec reponse de PHP en mode texte:
            //chargement des elements reçus dans la liste
            var affich_rep = http_request.responseText;

            if (affich_rep != "null") {
                Monstres = JSON.parse(affich_rep);

                newMonstre();
                IsAlive = true;

                document.getElementById("spinner").style.display = 'none';
                console.log("Monsters have been captured and are ready to be killed !");
            }
            else {
                document.getElementById("spinner").style.display = 'none';
                console.log("No monsters found !");
            }
        }
        else {
            document.getElementById("spinner").style.display = 'none';
            console.log('A problem has occured with the monsters : ' + http_request.status);
        }
    }
}

function traitementReponseSauvegarde(http_request) {
    if (http_request.readyState == 4) {
        if (http_request.status == 200) {
            // cas avec reponse de PHP en mode texte
            document.getElementById("spinner").style.display = 'none';
            console.log(http_request.responseText);

            if (Reset == true) {
                initialiseTheGame()
            }
        }
        else {
            document.getElementById("spinner").style.display = 'none';
            console.log('A problem has occured with the save : ' + http_request.status);
        }
    }
}

function checkSave() {
    saveSelected = ""
    try {
        InfosCookie = JSON.parse(InfosCookie)
        var pointUser = User.money * 1 + User.atlesianKnight130 * 10 + User.atlesianKnight200 * 100 + User.atlesianPaladin290 * 1000 + User.spiderDroid * 10000 + User.spiderDroid * 100000 + User.aP06 * 1000000 + User.colossus * 10000000
        var pointCookie = InfosCookie.Money * 1 + InfosCookie.AtlesianKnight130 * 10 + InfosCookie.AtlesianKnight200 * 100 + InfosCookie.AtlesianPaladin290 * 1000 + InfosCookie.SpiderDroid * 10000 + InfosCookie.AP06 * 100000 + InfosCookie.Colossus * 1000000
        if (pointUser >= pointCookie) {
            saveSelected = "BDD"
        }
        else {
            saveSelected = "cookie"
        }
    }
    catch {
        saveSelected = "BDD"
    }

    return saveSelected
}

/////////////////////////////////////////////////////////////////
//                       EVENTS                                //
/////////////////////////////////////////////////////////////////

//ajout 1 chaque click 
function onClick() {
    if (IsAlive) {
        console.log("click")
        if (AtlesianKnight130Lvl + 1 < LPMonstre) {
            LPMonstre -= AtlesianKnight130Lvl + 1
        }
        else {
            LPMonstre = 0
            mortMonstre()
        }
        document.getElementById(Resolution.concat('', "LPMonstre")).style.width = LPMonstre / LPMonstreFull * 100 + "%"
    }
};

//Déblocage ou leveling des améliorations
function clickButtonUpgrade(nameUpgrade) {

    var ameliorations = [
        ["AtlesianKnight130", AtlesianKnight130Unlocked, "nombreAtlesianKnight130", 10, AtlesianKnight130Lvl, 1, "divAtlesianKnight130", "buttonAtlesianKnight130"],
        ["AtlesianKnight200", AtlesianKnight200Unlocked, "nombreAtlesianKnight200", 1000, AtlesianKnight200Lvl, 5, "divAtlesianKnight200", "buttonAtlesianKnight200"],
        ["AtlesianPaladin290", AtlesianPaladin290Unlocked, "nombreAtlesianPaladin290", 100000, AtlesianPaladin290Lvl, 10, "divAtlesianPaladin290", "buttonAtlesianPaladin290"],
        ["SpiderDroid", SpiderDroidUnlocked, "nombreSpiderDroid", 10000000, SpiderDroidLvl, 100, "divSpiderDroid", "buttonSpiderDroid"],
        ["AP06", AP06Unlocked, "nombreAP06", 1000000000, AP06Lvl, 1000, "divAP06", "buttonAP06"],
        ["Colossus", ColossusUnlocked, "nombreColossus", 100000000000, ColossusLvl, 10000, "divColossus", "buttonColossus"],
    ]

    ameliorations.forEach(amelioration => {
        if (amelioration[0] == nameUpgrade) {
            if (!amelioration[1]) {
                amelioration[1] = true;
                document.getElementById(Resolution.concat('', amelioration[2])).disabled = false
            }
            Money = Money - (amelioration[3] * (amelioration[4] + 1))
            document.getElementById(Resolution.concat('', "money")).innerHTML = "Money : " + nFormatter(Money, 3)
            amelioration[4] += 1
            document.getElementById(Resolution.concat('', amelioration[2])).innerHTML = "+" + nFormatter(amelioration[4] * amelioration[5], 3) + " clicks per second"
            document.getElementById(Resolution.concat('', amelioration[6])).style.display = 'block'
            console.log(amelioration[0] + " : " + amelioration[4]);
            document.getElementById(Resolution.concat('', amelioration[7])).innerHTML = "Upgrade for " + nFormatter(amelioration[3] * (amelioration[4] + 1), 3)
        }
    })

    AtlesianKnight130Unlocked = ameliorations[0][1]
    AtlesianKnight200Unlocked = ameliorations[1][1]
    AtlesianPaladin290Unlocked = ameliorations[2][1]
    SpiderDroidUnlocked = ameliorations[3][1]
    AP06Unlocked = ameliorations[4][1]
    ColossusUnlocked = ameliorations[5][1]

    AtlesianKnight130Lvl = ameliorations[0][4]
    AtlesianKnight200Lvl = ameliorations[1][4]
    AtlesianPaladin290Lvl = ameliorations[2][4]
    SpiderDroidLvl = ameliorations[3][4]
    AP06Lvl = ameliorations[4][4]
    ColossusLvl = ameliorations[5][4]

    checkAccesButton()
    calculCPS()
}

function newMonstre() {
    var selectedMonstre = Monstres[Math.floor(Math.random() * Math.floor(Monstres.length))]

    if (DeadMonster < 10) {
        LPMonstreFull = LPMonstre = selectedMonstre.lP * (DeadMonster * 0.1)
        MoneyValueMonstre = selectedMonstre.moneyValue * (DeadMonster * 0.5)
    }
    else {
        LPMonstreFull = LPMonstre = selectedMonstre.lP
        MoneyValueMonstre = selectedMonstre.moneyValue
    }
    document.getElementById("computer_imgMonstre").src = selectedMonstre.img
    document.getElementById("mobile_imgMonstre").src = selectedMonstre.img
    document.getElementById("computer_LPMonstre").style.width = LPMonstre / LPMonstreFull * 100 + "%"
    document.getElementById("mobile_LPMonstre").style.width = LPMonstre / LPMonstreFull * 100 + "%"
    document.getElementById("computer_nomMonstre").innerHTML = selectedMonstre.name
    document.getElementById("mobile_nomMonstre").innerHTML = selectedMonstre.name

}

function resetAccount() {
    if (confirm('Are you sure you want to reset your account ?')) {
        document.getElementById("spinner").style.display = 'initial';

        Reset = true

        AtlesianKnight130Unlocked = false
        AtlesianKnight130Lvl = 0
        AtlesianKnight200Unlocked = false
        AtlesianKnight200Lvl = 0
        AtlesianPaladin290Unlocked = false
        AtlesianPaladin290Lvl = 0
        SpiderDroidUnlocked = false
        SpiderDroidLvl = 0
        AP06Unlocked = false
        AP06Lvl = 0
        ColossusUnlocked = false
        ColossusLvl = 0
        DeadMonster = 0
        Money = 0

        WantCookie = getCookie("want_cookie")
        if (WantCookie == "yes") {
            sauvegarde()
        }
        else {
            Reset = false
            initialiseTheGame()
        }
    }
}

//Petit cheat des familles parce qu'on aime bien avoir des gros scores
function cheat() {
    admin = false;
    User.roles.forEach(role => { if (role == "ROLE_ADMIN") { admin = true } });

    if (admin) {
        Money = 9999999999999999999999

        //variables pour les améliorations
        AtlesianKnight130Unlocked = true
        AtlesianKnight130Lvl = 999
        document.getElementById(Resolution.concat('', "nombreAtlesianKnight130")).innerHTML = "+" + nFormatter(AtlesianKnight130Lvl * 1, 3) + " clicks per click"
        document.getElementById(Resolution.concat('', "buttonAtlesianKnight130")).innerHTML = "Upgrade for " + nFormatter(10 * (AtlesianKnight130Lvl + 1), 3)
        document.getElementById(Resolution.concat('', "divAtlesianKnight130")).style.display = 'block'
        AtlesianKnight200Unlocked = true
        AtlesianKnight200Lvl = 999
        document.getElementById(Resolution.concat('', "nombreAtlesianKnight200")).innerHTML = "+" + nFormatter(AtlesianKnight200Lvl * 5, 3) + " clicks per second"
        document.getElementById(Resolution.concat('', "buttonAtlesianKnight200")).innerHTML = "Upgrade for " + nFormatter(1000 * (AtlesianKnight200Lvl + 1), 3)
        document.getElementById(Resolution.concat('', "divAtlesianKnight200")).style.display = 'block'
        AtlesianPaladin290Unlocked = true
        AtlesianPaladin290Lvl = 999
        document.getElementById(Resolution.concat('', "nombreAtlesianPaladin290")).innerHTML = "+" + nFormatter(AtlesianPaladin290Lvl * 10, 3) + " clicks per second"
        document.getElementById(Resolution.concat('', "buttonAtlesianPaladin290")).innerHTML = "Upgrade for " + nFormatter(100000 * (AtlesianPaladin290Lvl + 1), 3)
        document.getElementById(Resolution.concat('', "divAtlesianPaladin290")).style.display = 'block'
        SpiderDroidUnlocked = true
        SpiderDroidLvl = 999
        document.getElementById(Resolution.concat('', "nombreSpiderDroid")).innerHTML = "+" + nFormatter(SpiderDroidLvl * 100, 3) + " clicks per second"
        document.getElementById(Resolution.concat('', "buttonSpiderDroid")).innerHTML = "Upgrade for " + nFormatter(10000000 * (SpiderDroidLvl + 1), 3)
        document.getElementById(Resolution.concat('', "divSpiderDroid")).style.display = 'block'
        AP06Unlocked = true
        AP06Lvl = 999
        document.getElementById(Resolution.concat('', "nombreAP06")).innerHTML = "+" + nFormatter(AP06Lvl * 1000, 3) + " clicks per second"
        document.getElementById(Resolution.concat('', "buttonAP06")).innerHTML = "Upgrade for " + nFormatter(1000000000 * (AP06Lvl + 1), 3)
        document.getElementById(Resolution.concat('', "divAP06")).style.display = 'block'
        ColossusUnlocked = true
        ColossusLvl = 999
        document.getElementById(Resolution.concat('', "nombreColossus")).innerHTML = "+" + nFormatter(ColossusLvl * 10000, 3) + " clicks per second"
        document.getElementById(Resolution.concat('', "buttonColossus")).innerHTML = "Upgrade for " + nFormatter(100000000000 * (ColossusLvl + 1), 3)
        document.getElementById(Resolution.concat('', "divColossus")).style.display = 'block'
        checkAccesButton()
        calculCPS()
    }
    else {
        alert("You can't cheat if you're not an Admin ;)");
    }

}


